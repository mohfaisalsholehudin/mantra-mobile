package id.go.pajak.mantra;

import android.net.Uri;

public interface RNEditTextOnPasteListener {
    void onPaste(Uri itemUri);
}
